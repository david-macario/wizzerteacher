from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CreateUserForm, UserChangeForm

# Register your models here.

from .models import User, Subject, Exercise, Speciality, Bootcamp

class CustomUserAdmin(UserAdmin):
    add_form = CreateUserForm
    form = UserChangeForm
    model = User
    list_display = ['username', 'email', 'date_of_birth', 'phone_number', 'school', 'school_year', 'school_level', 'speciality', 'github_link', 'is_admin']


admin.site.register(User)
admin.site.register(Subject)
admin.site.register(Exercise)
admin.site.register(Speciality)
admin.site.register(Bootcamp)
