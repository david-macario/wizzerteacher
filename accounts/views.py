from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages



# Create your views here.
from .forms import CreateUserForm, UserChangeForm
from .models import User, Speciality, Bootcamp, Subject, Exercise


def home(request):
    user = request.user

    if user.is_authenticated:
        return render(request, 'accounts/home.html', context={'logged_in': True})
    else:
        return render(request, 'accounts/home.html', context={'logged_in': False})
    

@login_required(login_url='/login')
def profile(request, user_id):
    user = User.objects.get(id=user_id)
    context = {'user': user, 'logged_in': True}
    return render(request, 'accounts/profile.html', context)

@login_required(login_url='/login')
def lessons(request):
    return render(request, 'accounts/lessons.html')

@login_required(login_url='/login')
def exercises(request):
    return render(request, 'accounts/exercises.html')

def loginPage(request):
    if request.user.is_authenticated:
        return render(request, 'accounts/home.html', context={'logged_in': True})
    
    context = {}
    
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        username = User.objects.get(email=email.lower()).username
        user = authenticate(request, username=username, password=password)


        if user is not None:
            login(request, user)
            return render(request, 'accounts/home.html', context={'logged_in': True})
        else:
            messages.info(request, 'Email or password is incorrect')
            return render(request, 'accounts/login.html', context)

    
    return render(request, 'accounts/login.html', context)


def logoutUser(request):
    logout(request)
    return redirect('/login')

def register(request):
    if request.user.is_authenticated:
        return render(request, 'accounts/home.html', context={'logged_in': True})
    
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)

        print(form.errors)

        if form.is_valid():
            form.save()
            return redirect('/login')

    context = {'form': form}
    return render(request, 'accounts/register.html', context)

@login_required(login_url='/login')
def dashboard(request):
    user = request.user
    

    if user.is_admin: 
        users = User.objects.all()
        bootcamps = Bootcamp.objects.all()
        lessons = Subject.objects.all()
        exercises = Exercise.objects.all()
        specialities = Speciality.objects.all()
        context = {'user': user, 
                   'users': users, 
                   'bootcamps': bootcamps, 
                   'lessons': lessons, 
                   'exercises': exercises, 
                   'specialities': specialities,
                   'logged_in': True}
        return render(request, 'accounts/dashboard_admin.html', context)
    

    else:
        speciality = user.speciality.all().first()
        bootcamps = Bootcamp.objects.all().filter(students=user)
        lessons = Subject.objects.all().filter(speciality=user.speciality.all().first())
        exercises = Exercise.objects.all().filter(speciality=user.speciality.all().first())
        context = {'user': user, 'bootcamps': bootcamps, 'lessons': lessons, 'exercises': exercises, 'speciality': speciality, 'logged_in': True}
        return render(request, 'accounts/dashboard_user.html', context)
       
