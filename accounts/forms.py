from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django import forms

from .models import User

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2', 'date_of_birth', 'phone_number', 'school', 'school_year', 'school_level', 'speciality', 'github_link']

class UserChangeForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'date_of_birth', 'phone_number', 'school', 'school_year', 'school_level', 'speciality', 'github_link'] 