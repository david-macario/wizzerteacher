from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class Bootcamp(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100, null=True)
    students = models.ManyToManyField('User', related_name='bootcamps', blank=True)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name

class User(AbstractUser):
    password = models.CharField(max_length=100, blank=True, default='password')
    username = models.CharField(max_length=100, unique=True, default='username', blank=True)
    is_admin = models.BooleanField(default=False)
    date_of_birth = models.DateField(blank=True, null=True)
    phone_number = models.CharField(max_length=10, null=True, blank=True)
    school = models.CharField(max_length=100, null=True, blank=True )
    school_year = models.CharField(max_length=100, null=True, blank=True)
    school_level = models.CharField(max_length=100, null=True, blank=True)
    speciality = models.ManyToManyField('Speciality', related_name='users', blank=True)
    global_progress = models.IntegerField(default=0, null=True, blank=True)
    daily_progress = models.IntegerField(default=0, null=True, blank=True)
    github_link = models.CharField(max_length=100, null=True, blank=True)
    bootcamp = models.ManyToManyField('Bootcamp', related_name='users', blank=True)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.username

    
class Subject(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100, null=True)
    date_created = models.DateField(auto_now_add=True)
    speciality = models.ManyToManyField('Speciality', related_name='subjects', blank=True)

    def __str__(self):
        return self.name
    
class Exercise(models.Model):
    STATUS = (
        ('En attente', 'En attente'),
        ('En cours', 'En cours'),
        ('Terminé', 'Terminé'),
    )
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.SET_NULL, null=True)
    hints = models.CharField(max_length=255, null=True)
    bootcamp_day = models.IntegerField(default=0, null=True)
    speciality = models.ForeignKey('Speciality', on_delete=models.SET_NULL, null=True)
    passed_by = models.ManyToManyField(User, related_name='passed_exercises', blank=True)
    status = models.CharField(max_length=100, null=True, choices=STATUS)
    date_created = models.DateField(auto_now_add=True)
    


    def __str__(self):
        return self.name
    
class Speciality(models.Model):
    name = models.CharField(max_length=100)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name
