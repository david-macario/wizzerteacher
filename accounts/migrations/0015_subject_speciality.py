# Generated by Django 5.0.1 on 2024-01-25 10:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0014_remove_user_bootcamp_user_bootcamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='subject',
            name='speciality',
            field=models.ManyToManyField(blank=True, related_name='subjects', to='accounts.speciality'),
        ),
    ]
